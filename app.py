from flask import Flask, render_template, request, redirect
from flask_pymongo import pymongo
from bson.objectid import ObjectId


CONNECTION_STRING = "mongodb+srv://dbUser:RStiaVXznPzdNLMi@cluster0-jwlck.mongodb.net/test"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('Todo')
user_collection = pymongo.collection.Collection(db, 'tasks')
app = Flask(__name__)


@app.route('/index', methods=['POST', 'GET'])
def index():
	if request.method == "POST":
		#Recupère données input
		data = request.form.get("task")
		#Insère cette donnée en BDD
		db.tasks.insert_one({"tasks":data})
	#Recupère les données de la BDD
	results = db.tasks.find()
	#Tableau de données où l'on injecte les données dans la boucle for
	tasks = []
	for liste in results:
		tasks.append({'id':liste['_id'],'tasks': liste['tasks']})

	return render_template("index.html", tasks=tasks)


@app.route('/delete', methods=['POST'])
def delete():
	key = request.form['id']
	db.tasks.remove({'_id':ObjectId(key)})
	return redirect("/index")

if __name__ == "__main__":
	app.run(debug=True)
